var XLSX = require('XLSX');

function datenum(v, date1904) {
	if(date1904) v+=1462;
	var epoch = Date.parse(v);
	return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}

function sheetFromData(data){
  var ws = {};
  var keys = [];
  keys.push('No.');
  var column = {v : 'No.', t : 's'};
  var ref = XLSX.utils.encode_cell({c:0,r:0});
  ws[ref] = column;
  var cc = 1;
  for(var k in data[0]){
    keys.push(k);
    var c = {v : k, t : 's'};
    var r = XLSX.utils.encode_cell({c:cc,r:0});
    ws[r] = c;
    ++cc;
  }
	var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
	for(var R = 0; R != data.length; ++R) {
		for(var C = 0; C != keys.length; ++C) {
			if(range.s.r > R) range.s.r =R;
			if(range.s.c > C) range.s.c = C;
			if(range.e.r < (R+1)) range.e.r = (R+1);
			if(range.e.c < C) range.e.c = C;
			var cell, cell_ref;
      if (C === 0) {
        cell = {v : R+1, t : 'n'};
        cell_ref = XLSX.utils.encode_cell({c:C,r:(R+1)});
      } else {
        cell = {v: data[R][keys[C]] };
  			if(cell.v == null) {
          cell.v = '';
        }
  			cell_ref = XLSX.utils.encode_cell({c:C,r:(R+1)});

  			if(typeof cell.v === 'number') cell.t = 'n';
  			else if(typeof cell.v === 'boolean') cell.t = 'b';
  			else if(cell.v instanceof Date) {
  				cell.t = 'n'; cell.z = XLSX.SSF._table[14];
  				cell.v = datenum(cell.v);
  			}
  			else cell.t = 's';
      }
			ws[cell_ref] = cell;
		}
	}
	if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	return ws;
}

function Workbook() {
	if(!(this instanceof Workbook)) return new Workbook();
	this.SheetNames = [];
	this.Sheets = {};
}

exports.getFirstSheet = function(name){
	var wb = XLSX.readFile(name + '.xlsx');
	return wb.Sheets[wb.SheetNames[0]];
}

exports.writeRowsToXL = function (rows, sheetName, newWorkbookName){
  var wb = new Workbook(), ws = sheetFromData(rows);
  wb.SheetNames.push(sheetName);
  wb.Sheets[sheetName] = ws;
  /* write file */
  XLSX.writeFile(wb, newWorkbookName);
};
